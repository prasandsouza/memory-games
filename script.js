const gameContainer = document.getElementById("game");
const results = document.getElementById('final-result');
const replay = document.getElementById('replay-button');
const movesCounting = document.getElementById('moves');
const startinggame = document.getElementById('startgame');
const firstPage = document.getElementById('startingPage');
const gamingPage = document.getElementById('wholecontainer');
const finalResult = document.getElementById('final-results');
const replaybutton = document.getElementById('replay');
const winningMoves = document.getElementById('winning-moves');


const localStoragescore = window.localStorage.getItem('highestscore')
const highestScore = document.getElementById('highest-score');

if (localStoragescore == null) {
  highestScore.textContent = 'Highest Score : 0';
} else {
  highestScore.textContent = `Highest Score : ${localStoragescore}`;
}
console.log(highestScore)

const COLORS = [
     "red",
     "blue",
     "green",
     "orange",
     "purple",
     "maroon",
     "olive",
     "teal",
     "aqua",
     "silver",
];

function shuffle(array) {
     let counter = array.length;
     while (counter > 0) {
          let index = Math.floor(Math.random() * counter);
          counter--;
          let temp = array[counter];
          array[counter] = array[index];
          array[index] = temp;
     }
     return array;
}
let shuffledColors = shuffle(COLORS);
let totalColors = [...shuffledColors, ...shuffledColors];
let counter = 0;
let firstCard = "";
let secondCard = "";

function createDivsForColors(colorArray) {
     for (let color of colorArray) {
          const newDiv = document.createElement("div");
          newDiv.classList.add(color);
          newDiv.style.backgroundColor = "grey";
          newDiv.addEventListener("click", handleCardClick);
          gameContainer.append(newDiv);
     }
}
let totalCount = 0;
let movesCount = 0;
function handleCardClick(event) {
    movesCount++
    movesCounting.innerHTML = 'Moves :'+movesCount;
     let color = event.target.className;
     if (event.target.style.backgroundColor === "grey") {
          event.target.style.backgroundColor = color;
     }
     if (counter === 0) {
          firstCard = event.target;
          counter++;
          event.target.removeEventListener('click', handleCardClick)
     } 
     else {
          secondCard = event.target;
          counter = 0;
          firstCard.style.pointerEvents = 'auto'
          if (firstCard.style.backgroundColor !== secondCard.style.backgroundColor) 
          {
               setTimeout(() => {
                    firstCard.addEventListener('click',handleCardClick)
                    firstCard.style.backgroundColor = "grey";
                    secondCard.style.backgroundColor = "grey";
                    event.preventDefault();
               }, 500);
          }
          if(firstCard.style.backgroundColor==secondCard.style.backgroundColor){
            totalCount = totalCount + 1;
            firstCard.style.pointerEvents = 'none'
            secondCard.style.pointerEvents = 'none'

          }
     }

     if(totalCount==10){
      setTimeout(() => {
        gamingPage.style.display = 'none';
        finalResult.style.display='block';
        winningMoves.innerHTML = 'Your total move :'+movesCount;
        if(movesCount<localStoragescore){
        localStorage.setItem('highestscore',movesCount)
        }
      }, 800);
     }
}
console.log(movesCount)
startinggame.addEventListener('click',()=>{
  
  firstPage.style.display = 'none';
  gamingPage.style.display = 'block'
  // console.log('hello');
})
createDivsForColors(totalColors);